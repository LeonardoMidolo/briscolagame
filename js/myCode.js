var CarteUt;
var Mano;
var Dati=[];

function MostraModel()
{
    DisegnaInfo();
    $('#myModal').modal('show');
}
function IniziaPartita() {
    Mano=0;
    CarteUt=[];
	Dato=[];
    $.get(ws_hostname + "/Inizializza", function(data) {

        //console.log(data);
        CarteUt = data;
        DisegnaCarte(data);
    });
}
function DisegnaInfo()
{
          $.get(ws_hostname + "/RiceviFile", function(data) { 
        //console.log(data[2]);
       var x = data.split("\r\n");
        console.log(x);
        CreaPagina(x);
    });
    
}
function CreaPagina(x)
{   $("#Contenuti").empty();
    var Fine = 42; Somma = 42;
    var Mano = 0; var Indice = 0;
    NumeroPartite=1;
   var riga = ""; 
    for(var i=0; i<x.length; i++)
    {
		console.log(i);
        if(i==Fine||i==0) // Ad ogni inzio di partita
        {  
			Dati[Indice]=i;
			Indice++;
			console.log("Dentro Fine " + i)
            var tbl = '<table id="tbl'+i+'" class="table table-striped"><th>#</th><th>Mossa</th></thead>';
            var div = '<div id="div'+i+'" class="collapse table-responsive"></div>'
            var str ='<button class="btn btn-danger collapsed" data-toggle="collapse" data-target="#div'+i+'">Partita n. '+ NumeroPartite +'</button>';
            $("#Contenuti").append(str);
            $("#Contenuti").append(div);
            $("#div"+i+"").append(tbl);
			Fine = Fine +42;
			NumeroPartite++;
        }
    }
 var s = 0;
 var Limite = 40;
 console.log(x);
 	for(var z = 1; z<x.length; z++)
 	{
		
		if(Mano==40)
		{
			Mano = 0;
			$("#tbl"+Dati[s++]+"").append(riga);
		}
		else
		{
			Mano++;
	 		riga += "<tr>";
     		riga += "<td>Mano "+ Mano + "</td>";
     		riga += "<td>"+x[z]+"</td>";
     		riga += "</tr>";	
			
		}
	}
}

function DisegnaCarte(Carte, Vinto) {

    console.log(Carte);
    var Briscola;
    for (var i = 0; i < Carte.length; i++) {
        if (Carte[i].BriscolaCarta == true) {
            Briscola = Carte[i];
            Carte.splice(i, 1);
        }
    }
    if (Briscola == null) // La briscola è stata pescata
    {
        $('#BRIS').attr('src', 'image/01_vuot.png');
    } else {
        $('#BRIS').attr('src', '' + Briscola.PercorsoImmagine + '');
    }

    for (var i = 0; i < 3; i++) {
        console.log(Carte.length);
        if(Carte[i]==null)
        {
            $('#' + i + '').attr('src', 'image/01_vuot.png');
            $('#' + i + '').attr('onclick', '');

        }
        else
        {
            $('#' + i + '').attr('src', '' + Carte[i].PercorsoImmagine + '');
        
        if (Vinto) 
        {
            $("#" + i + "").attr('onclick', 'ScartaCarta2("' + Carte[i].ID + '")');
        } 
            else {
            $("#" + i + "").attr('onclick', 'ScartaCarta("' + Carte[i].ID + '")');

        }
        }
      
    }
}
function NominaVincitore(Vincitore)
{
    if (Vincitore == "Client") 
    {
        alert("Complimenti hai vinto");
    } 
    else if (Vincitore == "Server") 
    {
        alert("Hai perso questa partita");
    } 
     else 
     {
        alert("Partita finita in pareggio");
     }
}
function ScartaCarta(ID) {
    Mano = Mano + 1;
    for (var i = 0; i < CarteUt.length; i++) {
        if (CarteUt[i].ID == ID) {
            $("#8").attr('src', '' + CarteUt[i].PercorsoImmagine + '');
            $("#" + i + "").prop("src", "image/01_vuot.png"); //Rimuovo immagine quando la scarto
            $("#" + i + "").prop("onclick", null);
        }
    }
    $.get(ws_hostname + "/GiocaClient/" + ID + "", function(data) { //CLIENT-SERVER

             
            CarteUt = data.Carte[0]; //Aggiorno le carte dell'utente
            $("#6").attr('src', '' + data.PercorsoImmagine + ''); //Carta giocata dal server nella mano corrente


            if (data["Vincitore"] == "Carta1") {

                $("#StatoMano").text("Hai vinto questa mano");

                setTimeout(function() {
                    DisegnaCarte(CarteUt, false);
                    $("#8").prop("src", "");
                    $("#6").prop("src", "");
                    $("#StatoMano").text("Gioca");
                }, 500);
            } else 
            {
                $("#StatoMano").text("Hai perso questa mano");
                setTimeout(function() {
                    DisegnaCarte(CarteUt, true);
                    $("#6").attr('src', '' + data.ProssimaCartaServer.PercorsoImmagine + ''); //Mostro la carta che gioca il server
                    $("#8").prop("src", "");
                    $("#StatoMano").text("Gioca");
                }, 500);
            }
        if (data["VincitorePartita"] != null) 
        {
            NominaVincitore(data["VincitorePartita"]);  
        }  
    });
}

function ScartaCarta2(ID) {
    Mano = Mano + 1;

    for (var i = 0; i < CarteUt.length; i++) 
    {
        if (CarteUt[i].ID == ID) {
            $("#8").attr('src', '' + CarteUt[i].PercorsoImmagine + '');
            $("#" + i + "").prop("src", "image/01_vuot.png"); //Rimuovo immagine dalle carte quando la scarto
            $("#" + i + "").prop("onclick", null);
        }
    }
    $.get(ws_hostname + "/RiceviCarta/" + ID + "", function(data) {
        
        CarteUt = data.Carte[0];
        if (data["Vincitore"] == "Carta1") 
        {
            $("#StatoMano").text("Hai perso questa mano");

            setTimeout(function() {
                DisegnaCarte(CarteUt, true);
                if(data.ProssimaCartaServer==null)
                {
                    $("#6").attr('src', '');
                    $("#StatoMano").text("Partita finita");
                    $("#8").prop("src", "");
                }
                else
                {
                     $("#6").attr('src', '' + data.ProssimaCartaServer.PercorsoImmagine + '');
                     $("#8").prop("src", "");
                     $("#StatoMano").text("Gioca");
                }
               
            }, 500);
        } else 
        {

            $("#StatoMano").text("Hai vinto questa mano");
            setTimeout(function() {
                DisegnaCarte(CarteUt, false);
                $("#6").prop("src", "");
                $("#8").prop("src", "");
                $("#StatoMano").text("Gioca");
            }, 500);
        }
        if (data["VincitorePartita"] != null) 
        {
            NominaVincitore(data["VincitorePartita"]);  
        }
    });
}