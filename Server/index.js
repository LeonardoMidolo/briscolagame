var express = require('express');
//var csv = require('fast-csv');
var fs = require('fs');
var Mazzo =[];
var app = express();

////VARIABILI "GLOBALI" /////////////////
var Mano;                        //
var CarteTmpServer;              //
var CarteTmpUtente;               //
var CartaScartataServer;          //
var PuntiServer; var PuntiClient;
var data =""//
///////////////////////////////////////
/// npm install -g tsd
//var ws = fs.createWriteStream('Storico.csv');

console.log('In ascolto sulla porta 8080');

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/', function(req, res) 
{
	console.log(CarteTmpUtente.length);
  	res.send('Server ON');
});
app.get('/RiceviFile', function(req, res)
{
        console.log("Richiesta invio file")

        var Testo;
    fs.readFile('Storico.txt', 'utf8', function(err, contents) {
    res.send(contents);
});
     
    
});
app.get('/Inizializza', function(req, res) 
{
	 Mazzo = require('./Mazzo.json'); 
    Mano=0;
    CarteTmpServer=[]; 
    CarteTmpUtente=[];
    CartaScartataServer={};
    PuntiServer=0;
    PuntiClient=0;
	
    var Data =  new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
     data += Data + " Inizio partita  \r\n";
	MescolaArray(Mazzo);

	CarteTmpUtente = CartePrimaMano(Mazzo);
    res.send(CarteTmpUtente);
 	
});
app.get('/GiocaClient/:IdCarta', function(req, res) //  CLIENT- SERVER
{
	var CodiceCarta = req.params.IdCarta;
	var CartaClient=ScartaCarta(CodiceCarta,Mazzo);
	var CartaServer = ScartaCartaServer();
    
	var Result = ValutaVincitoreMano(CartaClient,CartaServer);
	 
	if(Result.Vincitore=="Carta1")
	{
		PuntiClient = PuntiClient + Result.Punti;
        console.log("Vince Client /n Punti: " + PuntiClient);
        if(Mano==17)
        {
            CarteTmpServer.push(PescaCarta("Server",true)); // Ordine di pescaggio.. Chi perde prende la briscola
            CarteTmpUtente.push(PescaCarta("Client",false));	
        }
        else if(Mano < 17)
        {
            CarteTmpUtente.push(PescaCarta("Client",false));	
            CarteTmpServer.push(PescaCarta("Server",false));
        }
	}
	else  //Se vince il server
	{
		PuntiServer = PuntiServer+ Result.Punti;
        console.log("Vince Server /n Punti: " + PuntiServer);
        if(Mano==17)
        {
            CarteTmpUtente.push(PescaCarta("Client",true));
            CarteTmpServer.push(PescaCarta("Server",false));
        }
        else if(Mano < 17)
        {
            CarteTmpServer.push(PescaCarta("Server",false));
            CarteTmpUtente.push(PescaCarta("Client",false));	
        }
		if(Mano < 21)
        {
          CartaScartataServer = ScartaCartaServer();            //Preparo la carta che scarta il server la mano successiva alla seguente
		  Result["ProssimaCartaServer"] = CartaScartataServer;  //Potrei mettere solo il percorso dell'immagine - CARTA MANO SUCCESSIVA
        }
	}
	Result["PercorsoImmagine"]=CartaServer.PercorsoImmagine;  //Percorso dell'immagine giocata dal server- MANO CORRENTE
	Result["Carte"]=[];
	Result["Carte"].push(CarteTmpUtente); //Aggiorno le carte dell'utente
	
    Result["PuntiServer"] = PuntiServer;
    Result["PuntiClient"] = PuntiClient;
	
    if(Mano==20&&PuntiClient+PuntiServer==120)
    {
        Result["VincitorePartita"] = ValutaVincitorePartita();
    }
    res.send(Result);
});
app.get('/RiceviCarta/:IdCarta', function (req,res) //SERVER-CLIENT
{
	var CodiceCarta = req.params.IdCarta;
    var CartaClient=ScartaCarta(CodiceCarta,Mazzo);

	console.log("Carta scartata server = "+ CartaScartataServer.ID);
    console.log("Carta scartata client= "+ CartaClient.ID);
	var Result = ValutaVincitoreMano(CartaScartataServer,CartaClient);
	
	if(Result.Vincitore=="Carta1")
	{  
        
        PuntiServer = PuntiServer+ Result.Punti;
      
        console.log("Vince Server - Punti: " + PuntiServer);
		
        if(Mano==17)
        {
            CarteTmpUtente.push(PescaCarta("Client",true));	
            CarteTmpServer.push(PescaCarta("Server",false));

        }
        else if(Mano < 17) //Preparo la carta per la mano Successiva
        {
            CarteTmpServer.push(PescaCarta("Server",false));
            CarteTmpUtente.push(PescaCarta("Client",false));	
        }
        
		
        if(Mano<20) // Non scarto nessuna carta
        {
            CartaScartataServer = ScartaCartaServer();
            Result["ProssimaCartaServer"]=CartaScartataServer;
        }
	}
	else
	{
        PuntiClient = PuntiClient + Result.Punti;
        console.log("Vince Client - Punti: " + PuntiClient);
		
        if(Mano==17)
        {
            CarteTmpServer.push(PescaCarta("Server",true));
            CarteTmpUtente.push(PescaCarta("Client",false));	 // Pesco la briscola
           

        }
        else if(Mano < 17)
        {
            CarteTmpUtente.push(PescaCarta("Client",false));	
            CarteTmpServer.push(PescaCarta("Server",false));
        }
	}
    
    
	Result["Carte"]=[];
	Result["Carte"].push(CarteTmpUtente); 
	Result["PuntiServer"] = PuntiServer;
    Result["PuntiClient"] = PuntiClient;
    
    if(Mano==20&&PuntiClient+PuntiServer==120)
    {
        Result["VincitorePartita"] = ValutaVincitorePartita();
    }
    res.send(Result);
    
});
app.get('/Inizializza', function(req, res) 
{
	MescolaArray(Mazzo);
  res.send(CartePrimaMano(Mazzo));
 	
});

app.listen(8080);

function ValutaVincitorePartita()
{
     var path = "Storico.txt";
     var Data = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
    if(PuntiClient > PuntiServer)
    {
        data += Data + "Partita vinta da Client punti= "+PuntiClient+ " \r\n";
        fs.appendFile(path, data, function(error) {});
        return "Client";
    }
    else if (PuntiClient == PuntiServer)
    {
        data += Data + "Partita finita in parità punti= "+PuntiServer+ " \r\n";
        fs.appendFile(path, data, function(error) {});
        return "Parità";
    }
    else
    {
        data += Data + "Partita vinta da server punti= "+PuntiServer+ " \r\n";
        fs.appendFile(path, data, function(error) {});
        return "Server";
    }
    
     
}
    
function ScriviSuCsv(Nome,Id)
{
   
    var Data =  new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
    var path = "Storico.txt";
    var data = Data + " " + Nome+ " gioca carta n: " + Id +  " , \r\n";
    
    fs.appendFile(path, data, function(error) {});
}
function ScartaCarta(Codice,Mazzo)
{
    data += "Client scarta carta n. " + Codice + " \r\n";
	var Posizione = 0;
	var Carta;
	Mazzo.forEach(function(data){
		if(data.ID==Codice)
		{
			data.StatoCarta="Scartata";
			Carta = data;
            console.log("Client scarta carta n: "+data.ID);

		}
	});
	for(var i =0; i<CarteTmpUtente.length; i++)
	{
		if(CarteTmpUtente[i].ID==Codice)
		{
			Posizione=i;
		}
	}
    CarteTmpUtente.splice(Posizione,1);
	
	return Carta;
}
function ValutaVincitoreMano(Carta1,Carta2) //Prima carta giocata - Seconda carta giocata 
{
    Mano = Mano+1;
    console.log("Mano n. "+ Mano);
	//console.log(Carta1.ID + " di " + Carta1.Seme + " Valore= " + Carta1.Valore+"");
	//console.log(Carta2.ID + " di " + Carta2.Seme + " Valore= " + Carta2.Valore+"");
   
    var NumeroCarta1= Carta1.ID.toString().split('').pop();
    var NumeroCarta2= Carta2.ID.toString().split('').pop();
	var Punti = Carta1.Valore+Carta2.Valore;

	if(Carta1.Briscola==true || Carta2.Briscola==true)
	{
		if(Carta1.Briscola==true&&Carta2.Briscola==true)
		{
			if(Carta1.Valore==0&&Carta2.Valore==0)
			{
				if(NumeroCarta1>NumeroCarta2)
				{
					return {"Vincitore":"Carta1","Punti":Punti}
				}
				else
				{
					return {"Vincitore":"Carta2","Punti":Punti}
				}
			}
			else
			{
				if(Carta1.Valore>Carta2.Valore)
				{
					return {"Vincitore":"Carta1","Punti":Punti}//Vince Client
				}
				else
				{
					return {"Vincitore":"Carta2","Punti":Punti}
					// Vince server	
				}	
			}
		}
		else if (Carta1.Briscola==true&&Carta2.Briscola==false)
		{
			return {"Vincitore":"Carta1","Punti":Punti}
		}
		else
		{
			return {"Vincitore":"Carta2","Punti":Punti}
		}
	}
	else
	{
		if(Carta2.Seme != Carta1.Seme)
		{
			return {"Vincitore":"Carta1","Punti":Punti}
            
		}
		else if(Carta1.Valore==0 && Carta2.Valore==0) //Se le carte non hanno valore confronto il loro numero, anche se non influiscono sui punti influiscono sul pescaggio successivo
		{

			if(NumeroCarta1 > NumeroCarta2)
			{
				return {"Vincitore":"Carta1","Punti":Punti}
			}
			else
			{
				return {"Vincitore":"Carta2","Punti":Punti}
			}
		}
		else
		{
			if(Carta1.Valore>Carta2.Valore)
			{
				return {"Vincitore":"Carta1","Punti":Punti}
			}
			else
			{
				return {"Vincitore":"Carta2","Punti":Punti}
			}
		}
	}
}
   
function MescolaArray(array)  //Mescola il mazzo 
{
  var currentIndex = array.length, temporaryValue, randomIndex;

  while (0 !== currentIndex) {

    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
}
function CartePrimaMano(Mazzo) //Assegno le carte iniziali. Ordine= 3 Client- 3 Server - 1Briscola
{
	
	var CarteTmpUtente = [];
	for(var i =0; i<7; i++)
	{
		if(i<=2)
		{
			Mazzo[i].StatoCarta ="Utente";
			CarteTmpUtente.push(Mazzo[i]);
		}
		else if (i < 6)
		{
			Mazzo[i].StatoCarta = "Server";
			CarteTmpServer.push(Mazzo[i]);
			console.log("Dentro");
		}
		else
		{
			console.log("Si");
			Mazzo[i].BriscolaCarta = true;
			Mazzo[i].StatoCarta = "BriscolaCarta";
			CarteTmpUtente.push(Mazzo[i]);
			ImpostaBriscola(Mazzo[i],Mazzo);
		}
		
	}
	//console.log(CarteTmpUtente.length);
    return CarteTmpUtente;
}
function PescaCarta(Utente,Briscola) //GESTIRE ULTIMO PESCAGGIO
{
    console.log("Briscola: " + Briscola);
    if(Briscola)
    {
        console.log("Ultima carta");
        for(var j =0; j< CarteTmpUtente.length; j++)
        {
            if(CarteTmpUtente[j].BriscolaCarta)
            {
                CarteTmpUtente.splice(j,1); 
            }
        }
        for(var i=0; i< Mazzo.length; i++)
		{
			if(Mazzo[i].BriscolaCarta==true)
		   {
			  //console.log(Mazzo[i].StatoCarta);
               Mazzo[i].BriscolaCarta = false; // Rendo la birscola carta  non più birscola per il client 
			   Mazzo[i].StatoCarta=Utente;
               console.log("Ultima carta" + Mazzo[i].ID);
			   console.log(Mazzo[i].StatoCarta);
			   console.log("Carta pescata da :"+ Utente);
			   console.log(Mazzo[i].ID);
		   	   return Mazzo[i];
		   }
			
		}
    }
    else
    {
        for(var i=0; i< Mazzo.length; i++)
		{
			if(Mazzo[i].StatoCarta=="libera")
		   {
			  console.log(Mazzo[i].StatoCarta);

			   Mazzo[i].StatoCarta=""+Utente+"";
			   console.log(Mazzo[i].StatoCarta);
			   console.log("Carta pescata da :"+ Utente);
			   console.log(Mazzo[i].ID);
		   	   return Mazzo[i];
		   }	
		}
    }
}
function ScartaCartaServer() //Scarto carta a caso 
{
    for(var i=0; i<Mazzo.length; i++){ //Debug
	       if(Mazzo[i].StatoCarta == "Server"){
			 console.log("Server ha carta n " + Mazzo[i].ID);
           }
		  }
    console.log("Carte server " + CarteTmpServer.length);
	var NumeroRandom = Math.floor((Math.random() * CarteTmpServer.length) + 0); //QUESTI VALORI DEVONO CAMBIARE 
    while(CarteTmpServer[NumeroRandom]==null)
    {
        console.log("***************************************");
        console.log("Dentro il buggg");
        console.log("***************************************");
        NumeroRandom = Math.floor((Math.random() * CarteTmpServer.length) + 0)
    }
	var CartaTmp = CarteTmpServer[NumeroRandom];
	for(var i=0; i<Mazzo.length; i++){
		if(CartaTmp.ID==Mazzo[i].ID) ////ERRORE RANDOM 
		  {
            data += "Server scarta carta n. " + CartaTmp.ID + " \r\n";
            //ScriviSuCsv("Server",CartaTmp.ID);
			Mazzo[i].StatoCarta = "Scartata";
			CarteTmpServer.splice(NumeroRandom,1);//POSIZIONE
			console.log("Server scarta carta n " +  CartaTmp.ID);
		  }
    }
	
	return CartaTmp;
}
function ImpostaBriscola(Carta,Mazzo) 
{
	
	Mazzo.forEach(function (item){
	
		if(item.Seme == Carta.Seme)
		{
			item.Briscola = true;
		}
	});
}
